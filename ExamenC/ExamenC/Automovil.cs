﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ExamenC {

    class Automovil : Vehiculo
    {
        int anio;
        public Automovil(string marca, string modelo, int anio):base(marca, modelo)
        {
            this.anio = anio;
        }

        public void infoAutomovil()
        {
            infoVehiculo();
            Console.WriteLine("--->Año: "+anio);
        }
    }

}
