﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenC
{
    internal class Vehiculo
    {
        string marca;
        string modelo;

        public Vehiculo(string marca, string modelo)
        {
            this.marca = marca;
            this.modelo = modelo;
        }

        public void infoVehiculo()
        {
            Console.WriteLine("VEHICULO \n--->Marca: " + marca + "\n--->Modelo: " + modelo);
        }
    }
}
