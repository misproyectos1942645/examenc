﻿using System.Text.RegularExpressions;
using ExamenC;

internal class Program
{
    static void Main(string[] args)
    {
        /*Ejercicio 1*/
        manipulacionDeArrays();

        /*Ejercicio 2*/
        vehiculosYautomoviles();

        /*Ejercicio 3*/
        usarCalculadora();

        /*Ejercicio 4*/
        manipulaCadena();

        /*Ejercicio 5*/
        manipulaCadena2();
    }


    /*Ejercicio 1*/
    static void manipulacionDeArrays()
    {
        int[] enteros;
        enteros = pedirEnteros();
        manipularArray(enteros);
    }
    static int[] pedirEnteros()
    {
        int tamArray = 0;
        do
        {
            Console.WriteLine("Tamaño para el Array");
            tamArray = pedirNumero();
        } while (tamArray <= 0);

        int[] enteros = new int[tamArray];

        for (int i = 0; i < enteros.Length; i++)
        {
            Console.WriteLine("Numero "+(i+1));
            int numIntroducido = pedirNumero();
            enteros[i] = numIntroducido;
        }
        return enteros;
    }

    static void manipularArray(int[] enteros)
    {
        int sumaTotal = 0;
        foreach (int i in enteros)
        {
            sumaTotal += i;
        }
        Console.WriteLine("La suma de todos los números introducidos es: " + sumaTotal);
    }

    static int pedirNumero()
    {
        bool numValido = false;
        int numIntroducido = 0;
        do
        {
            Console.WriteLine("Introduce número: ");
            try
            {
                numIntroducido = int.Parse(Console.ReadLine());
                numValido = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Debe ser un número válido");
            }
        } while (!numValido);
        return numIntroducido;
    }



    /*Ejercicio 2*/
    static void vehiculosYautomoviles()
    {
        Automovil automovil = new Automovil("Ford","Mustang",2018);
        automovil.infoAutomovil();
    }



    /*Ejercicio 3*/
    static void usarCalculadora()
    {
        Calculadora calculadora = new Calculadora();
        int x = pedirNumero();
        int y = pedirNumero();
        calculadora.sumar(x, y);
        calculadora.restar(x, y);
        calculadora.multiplicar(x, y);
        calculadora.dividir(x, y);
    }




    /*Ejercicio 4*/
    static void manipulaCadena()
    {
        Console.WriteLine("Introduce una frase: ");
        string frase = Console.ReadLine();

        string fraseInvertida = InvertirPalabras(frase);
        Console.WriteLine(fraseInvertida);
    }

    static string InvertirPalabras(string cadena)
    {
        string[] palabras = cadena.Split(' ');
        Array.Reverse(palabras);
        string resultado = string.Join(' ', palabras);
        return resultado;
    }





    /*Ejercicio 5*/
    static void manipulaCadena2()
    {
        Console.WriteLine("Introduce una frase: ");
        string frase = Console.ReadLine();
        if (EsPalindromo(frase))
        {
            Console.WriteLine("Es un palíndromo");
        }
        else
        {
            Console.WriteLine("No es un palíndromo");
        }
    }

    static bool EsPalindromo(string cadena)
    {
        cadena = Regex.Replace(cadena.ToLower(), "[^a-zA-Z]", "");
        string cadenaInvertida = new string(cadena.Reverse().ToArray());
        return cadena == cadenaInvertida;
    }
}