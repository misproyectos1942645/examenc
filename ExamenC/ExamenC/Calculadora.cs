﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenC
{
    internal class Calculadora
    {
        int x;
        int y;
        public Calculadora()
        {

        }
        public void sumar(int x, int y)
        {
            int suma = x + y;
            Console.WriteLine("Suma: " + suma);
        }
        public void restar(int x, int y)
        {
            int resta = x - y;
            Console.WriteLine("Resta: " + resta);
        }
        public void multiplicar(int x, int y)
        {
            int multiplicación = x * y;
            Console.WriteLine("Multiplicación: " + multiplicación);
        }
        public void dividir(int x, int y)
        {
            int division = 0;
            try
            {
                division = x / y;
                Console.WriteLine("División: " + division);
            }catch (DivideByZeroException) {
                Console.WriteLine("No se puede dividir por 0");
            }
        }
    }
}
